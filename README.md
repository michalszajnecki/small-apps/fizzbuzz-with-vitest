# fizzBuzz with Vitest 

FizzBuzz benchmark targets four different solutions: two with console logs and two without them. The whole project showcases the benchmarking tool attached to Vitest and the speed implications of using console logs in unit tests.

![FizzBuzz benchmark results](docs/images/benchmark.webp)

## How to run

1. `npm i`
2. `npm run bench`

