// Example FizzBuzz solutions, each in separate class.

export class FBOne {
    public runTest(n: number): void {
        for (let i = 1; i <= n; i++) {
            if (i % 3 === 0 && i % 5 === 0) {
                console.log('FizzBuzz');
            } else if (i % 3 === 0) {
                console.log('Fizz');
            } else if (i % 5 === 0) {
                console.log('Buzz');
            } else {
                console.log(i);
            }
        }
    }
}

export class FBTwo {
    public runTest(n: number): void {
        for (let i = 1; i <= n; i++) {
            const fizz = i % 3 === 0;
            const buzz = i % 5 === 0;

            console.log(fizz && buzz ? 'FizzBuzz' : fizz ? 'Fizz' : buzz ? 'Buzz' : i);
        }
    }
}

export class FBThree {
    public runTest(n: number): string[] {
        const result: string[] = [];
        for (let i = 1; i <= n; i++) {
            const fizz = i % 3 === 0;
            const buzz = i % 5 === 0;

            result.push(fizz && buzz ? 'FizzBuzz' : fizz ? 'Fizz' : buzz ? 'Buzz' : i.toString());
        }
        return result;
    }
}

export class FBFour {
    public runTest(n: number): string {
        let output = '';
        for (let i = 1; i <= n; i++) {
            if (i % 15 === 0) {
                output += 'FizzBuzz\n';
            } else if (i % 3 === 0) {
                output += 'Fizz\n';
            } else if (i % 5 === 0) {
                output += 'Buzz\n';
            } else {
                output += i + '\n';
            }
        }
        return output;
    }
}
