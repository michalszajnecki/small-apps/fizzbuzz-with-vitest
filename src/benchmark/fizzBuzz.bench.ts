import { bench } from 'vitest';
import { FBOne, FBTwo, FBThree, FBFour } from '../fizzbuzzs/fizzBuzz';

bench('FBOne', () => {
    new FBOne().runTest(300);
});

bench('FBTwo', () => {
    new FBTwo().runTest(300);
});

bench('FBThree', () => {
    new FBThree().runTest(300);
});

bench('FBFour', () => {
    new FBFour().runTest(300);
});
